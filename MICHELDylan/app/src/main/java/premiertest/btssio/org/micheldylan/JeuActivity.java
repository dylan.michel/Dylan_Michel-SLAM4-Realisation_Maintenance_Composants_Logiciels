package premiertest.btssio.org.micheldylan;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

public class JeuActivity extends MainActivity {
    TextView pseudonyme;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jeu);
        String message = this.getIntent().getExtras().getString( "Joueur");
        pseudonyme = (TextView) super.findViewById(R.id.messagePseudo);
        pseudonyme.setText(pseudonyme.getText()+ message + " : ");
    }
}
