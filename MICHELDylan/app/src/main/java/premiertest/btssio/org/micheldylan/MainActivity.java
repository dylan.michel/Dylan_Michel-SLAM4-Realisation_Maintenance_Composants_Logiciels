package premiertest.btssio.org.micheldylan;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button departquizz;
    int num = 0;
    EditText pseudonyme;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        departquizz = (Button)this.findViewById(R.id.button_go);
        departquizz.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent_jeu = new Intent(this, JeuActivity.class);
        pseudonyme = (EditText)super.findViewById(R.id.editText);
        // on récupère la valeur saisie dans la zone de saisie
        String leprenom = pseudonyme.getText().toString();
        if(! leprenom.isEmpty()) {
            // on passe à l’intention cette valeur sous le « nom » « Joueur »
            intent_jeu.putExtra("Joueur",leprenom);
            this.startActivityForResult(intent_jeu, num);
        }
        else{
            Toast.makeText(this,"Vous n'avez pas rentré de pseudonyme !", Toast.LENGTH_LONG).show();
        }

    }
}
