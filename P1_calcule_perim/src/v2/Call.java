package v2;

import java.util.Scanner;
import java.util.ArrayList;
import java.util.List;


public class Call {
	public static Scanner set = new Scanner(System.in);
	static List<OptimizV2> resultat = new ArrayList<>();
	static boolean b = true;
	static int i;
	static int num = 0;
	static String vidage;
	
	public static int IntegerTest(int t)
	{
		
		try
		{
			t = set.nextInt();
			//throw new Exception("Vous n'avez pas saisie un int!"+  i);
			b=true;
		}
		catch(Exception e)
		{
			System.out.println("Vous n'avez pas saisie un int!");
			b = false;
			vidage = set.next();
		}
		return t;
	}
	public static float Float(float f)
	{
		
		try
		{
			f = set.nextFloat();
			//throw new Exception("Vous n'avez pas saisie un int!"+  i);
			b=true;
		}
		catch(Exception e)
		{
			System.out.println("Vous n'avez pas saisie un float!");
			b = false;
			vidage = set.next();
		}
		return f;
	}
	
	
	
	protected static void Menu() {
		boolean correct = true;
		boolean sortie = true;
		Scanner choix = new Scanner(System.in);


		do {

			System.out.println("Choisir un chiffre en fonction du choix néccessaires :\n\t1)Carré\n\t"
					+ "2)Triange issocel\n\t3)Cercle\n\t9)Quitter");
			try{
				//i = choix.nextInt();

				i = choix.nextInt();


				if((i != 1) && ( i!=2) && (i!=3) && (i!=9))
				{
					correct = false;	
					System.out.println("Saisie incorrect");
				}
			}
			catch(Exception e)
			{
				System.out.println("Vous avez saisie une lettre ?!!");
				vidage = choix.next();
			}

			if(correct)
			{
				if (i == 9) {
					sortie = false;
				} else if (i == 1) {
					Carre();
				} else if (i == 2) {
					Triangle();
				} else if (i == 3) {
					Rond();
				}
			}
			correct = true;
		} while (sortie);

		choix.close();
		System.out.println("Calcul");

		for(OptimizV2 line : resultat)
		{
			if(line instanceof Rectangle)
			System.out.println(line.toString());
			if(line instanceof Cercle)
				System.out.println(line.toString());
			if(line instanceof Triangle)
				System.out.println(line.toString());
			
		}
		

		System.out.println("\nBonne journée !");
	}
	
	
	
	protected static void Carre() {
		float c = 0;
		System.out.println("Vous avez choisis le carré !");
		// Scanner set = new Scanner(System.in);
		do {
			System.out.println("\tVeuillez indiquer la taille de son côté");
			c = Float(c);
		}while(!b);
		
		resultat.add(new Rectangle(c));
		System.out.println(resultat.get(num).LePerimetre());
		System.out.println(resultat.get(num).LaSurface());
		num++;
		

	}

	protected static void Triangle() {
		float ba = 0;
		float h = 0;
		// Scanner set = new Scanner(System.in);
		System.out.println(
				"\nLe calcule d'un triange isocèle.");
		do {
			System.out.println("Indiquez la première longeur connue (la base)");
			ba = Float(ba);

			System.out.println("\nPuis sa hauteur : ");

			h = Float(h);
		}while(!b);
		
		resultat.add(new Triangle(ba,h));
		//resultat.add(OptimizV2 new instanceof Triangle(i,x));
		System.out.println(resultat.get(num).LePerimetre());
		System.out.println(resultat.get(num).LaSurface());
		num++;


	}

	protected static void Rond() {
		float r = 0;
		// Scanner set = new Scanner(System.in);
		do {
			System.out.println("\nMaintenant pour un cercle, veuillez indiquer son rayon");
			r = Float(r);
		}while(!b);
			resultat.add(new Cercle(r));
			System.out.println(resultat.get(num).LePerimetre());
			System.out.println(resultat.get(num).LaSurface());
			num++;

		}
	}
