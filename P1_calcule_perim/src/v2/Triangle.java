package v2;

public class Triangle extends OptimizV2{
	float hauteur = 0;	

	public float getHauteur() {
		return hauteur;
	}	
	public void setX(float xhauteur)
	{
		this.hauteur=xhauteur;
	}	
	
	
	public Triangle(float uneLongueur, float uneHauteur) 
	{
		
		super(uneLongueur);
		this.hauteur = uneHauteur;
		this.perimetre = perim();
		this.surface = surf();
	}
	
	float perim() 
	{
		float perimetre;
		//perimetre = (float)Math.sqrt( ( (hauteur * hauteur) + ((longueur / 2) * (longueur / 2)))*2+longueur);
		perimetre = (float)Math.sqrt( Math.pow(longueur/2, 2) + Math.pow(hauteur, 2))*2 + longueur;
		return perimetre;
	}	
	float surf() 
	{
		float surface;
		surface = (float)((longueur * hauteur) / 2);
		return surface;
	};
	public String LaSurface() 
	{
		return "La surface du triangle et de : " + this.surface;
	}
	public String LePerimetre() 
	{
		return "Le périmètre du triangle et de : " + this.perimetre;	
	}
	
	public String toString() 
	{
		return "Pour un triangle nous avons en hauteur " + hauteur + super.toString() ;
	}
	
}
