package v2;

public class Cercle extends OptimizV2 {

	
	public Cercle(float uneLongueur) 
	{
		super(uneLongueur);
		this.perimetre = perim();
		this.surface = surf();
	}
	
	float perim() 
	{
		return (perimetre = (float)(2 * Math.PI * longueur));
	}	
	;
	float surf() 
	{
		return (float)(Math.PI * Math.pow(longueur, 2));
	};
	
	
	public String LaSurface() 
	{
		return "Sa surface est " + surface +"cm²";
	}
	public String LePerimetre() 
	{
		return "Son perimètre est "+ perimetre + "cm.";	
	}
	
	public String toString() 
	{
		return "Pour un cercle" + super.toString() ;
	}

}
