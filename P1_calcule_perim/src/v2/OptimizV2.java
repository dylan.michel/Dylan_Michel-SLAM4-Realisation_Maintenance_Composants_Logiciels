package v2;

public abstract class OptimizV2 {
	protected float longueur;	
	protected float perimetre;
	protected float surface;
	
	public float getLongueur() 
	{
		return longueur;
	}	
	public  float getPerimetre() 
	{
		return perimetre;
	}
	public  float getSurface() 
	{
		return surface;
	}

	public void setLongueur(float xlongueur)
	{
		this.longueur=xlongueur;
	}	
	public void setPerimetre(float xperimetre) 
	{
		this.perimetre=xperimetre;
	}
	public void setSurface(float xsurface) 
	{
		this.surface=xsurface;
	}
	
	public OptimizV2(float uneLongueur) 
	{
		this.longueur = uneLongueur;
	}
	
	public String toString()
	{
		return " nous avons" + longueur + "cm, \n\tson périmètre " + perimetre +"cm \n\tet sa surface " + surface + "cm\n";
	}
	
		abstract float perim();
		abstract float surf();
		abstract String LaSurface() ;
		abstract String LePerimetre();
		

}
