package v2;

public class Rectangle extends OptimizV2 {

	public Rectangle(float uneLongueur) 
	{
		super(uneLongueur);
		this.perimetre = perim();
		this.surface = surf();
	}
	
	float perim() 
	{
		float perimetre;
		perimetre = (float)(longueur * 4);
		return perimetre;
	}	
	float surf() 
	{
		float surface;
		surface = (float)(longueur * longueur);
		return surface;
	};
	
	public String LaSurface() 
	{
		return "La surface du carré est de : " + this.surface;
	}
	public String LePerimetre() 
	{
		return "Le périmètre du carré et de : " + this.perimetre;	
	}
	
	@Override
	public String toString() 
	{
		return "Pour un carré " + super.toString() ;
	}
}
