package v3;
import v2.OptimizV2;
import v2.Rectangle;
import v2.Triangle;
import v2.Cercle;

import java.awt.EventQueue;
import java.awt.TextArea;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;


public class TheDesign extends JFrame {
	private JTextField affCoteCarre;
	private JTextField affRayonCercle;
	private JTextField affHauteurTriangle;
	private JTextField affBaseTriangle;
	private int num = 0;
	private List<OptimizV2> resultat = new ArrayList<>();
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TheDesign frame = new TheDesign();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			
			
		});
	}

	/**
	 * Create the frame.
	 */
	public TheDesign() {
		getContentPane().setLayout(null);
		
		affCoteCarre = new JTextField();
		affCoteCarre.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent arg0) {
				
				if(!BoolFloat(affCoteCarre.getText() ) ) 
				{
					affCoteCarre.setText(null);
				}
			}
		});
		
		affCoteCarre.setBounds(397, 78, 114, 19);
		getContentPane().add(affCoteCarre);
		affCoteCarre.setColumns(10);
		
		affHauteurTriangle = new JTextField();
		affHauteurTriangle.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if(!BoolFloat(affHauteurTriangle.getText() ) ) 
				{
					affHauteurTriangle.setText(null);
				}
			}
		});
		
		affRayonCercle = new JTextField();
		affRayonCercle.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if(!BoolFloat(affRayonCercle.getText() ) ) 
				{
					affRayonCercle.setText(null);
				}
			}
		});
		affRayonCercle.setBounds(397, 244, 114, 19);
		getContentPane().add(affRayonCercle);
		affRayonCercle.setColumns(10);
		affHauteurTriangle.setBounds(397, 440, 114, 19);
		getContentPane().add(affHauteurTriangle);
		affHauteurTriangle.setColumns(10);
		
		affBaseTriangle = new JTextField();
		affBaseTriangle.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if(!BoolFloat(affBaseTriangle.getText() ) ) 
				{
					affBaseTriangle.setText(null);
				}
			}
		});
		affBaseTriangle.setBounds(397, 407, 114, 19);
		getContentPane().add(affBaseTriangle);
		affBaseTriangle.setColumns(10);
		
		JFormattedTextField affSurfaceTria = new JFormattedTextField();
		affSurfaceTria.setEditable(false);
		affSurfaceTria.setBounds(397, 471, 114, 19);

		getContentPane().add(affSurfaceTria);
		
		JFormattedTextField affPerimTria = new JFormattedTextField();
		affPerimTria.setEditable(false);
		affPerimTria.setBounds(397, 502, 114, 19);
		getContentPane().add(affPerimTria);
		
		JFormattedTextField affSurfaceCar = new JFormattedTextField();
		affSurfaceCar.setEditable(false);
		affSurfaceCar.setBounds(397, 109, 114, 19);
		getContentPane().add(affSurfaceCar);
		
		JFormattedTextField affPerimCar = new JFormattedTextField();
		affPerimCar.setEditable(false);
		affPerimCar.setBounds(397, 140, 114, 19);
		getContentPane().add(affPerimCar);
		
		JFormattedTextField affSurfaceCerc = new JFormattedTextField();
		affSurfaceCerc.setEditable(false);
		affSurfaceCerc.setBounds(397, 306, 114, 19);
		getContentPane().add(affSurfaceCerc);
		
		JFormattedTextField AffperimCerc = new JFormattedTextField();
		AffperimCerc.setEditable(false);
		AffperimCerc.setBounds(397, 275, 114, 19);
		getContentPane().add(AffperimCerc);
		
		JFormattedTextField surfaceCar = new JFormattedTextField();
		surfaceCar.setEditable(false);
		surfaceCar.setText("Surface :");
		surfaceCar.setBounds(271, 109, 114, 19);
	
		getContentPane().add(surfaceCar);
		
		JFormattedTextField perimCar = new JFormattedTextField();
		perimCar.setEditable(false);
		perimCar.setText("Perim :");
		perimCar.setBounds(271, 140, 114, 19);
		getContentPane().add(perimCar);
		
		JFormattedTextField cotCar = new JFormattedTextField();
		cotCar.setEditable(false);
		cotCar.setText("Coté :");
		cotCar.setBounds(271, 78, 114, 19);
		getContentPane().add(cotCar);
		
		JFormattedTextField rayonCerc = new JFormattedTextField();
		rayonCerc.setEditable(false);
		rayonCerc.setText("Rayon :");
		rayonCerc.setBounds(271, 244, 114, 19);
		getContentPane().add(rayonCerc);
		
		JFormattedTextField surfCerc = new JFormattedTextField();
		surfCerc.setEditable(false);
		surfCerc.setText("Surface :");
		surfCerc.setBounds(271, 275, 114, 19);
		getContentPane().add(surfCerc);
		
		TextArea listeRegion = new TextArea();
		listeRegion.setEditable(false);
		listeRegion.setBounds(523, 12, 378, 691);
		getContentPane().add(listeRegion);
		
		
		JFormattedTextField perimCerc = new JFormattedTextField();
		perimCerc.setEditable(false);
		perimCerc.setText("Perim :");
		perimCerc.setBounds(271, 306, 114, 19);
		getContentPane().add(perimCerc);
		
		JFormattedTextField hauteurTri = new JFormattedTextField();
		hauteurTri.setEditable(false);
		hauteurTri.setText("Hauteur :");
		hauteurTri.setBounds(271, 440, 114, 19);
		getContentPane().add(hauteurTri);
		
		JFormattedTextField surfTri = new JFormattedTextField();
		surfTri.setEditable(false);
		surfTri.setText("Surface :");
		surfTri.setBounds(271, 471, 114, 19);
		getContentPane().add(surfTri);
		
		JFormattedTextField perimTri = new JFormattedTextField();
		perimTri.setEditable(false);
		perimTri.setText("Perim :");
		perimTri.setBounds(271, 502, 114, 19);
		getContentPane().add(perimTri);
		
		JFormattedTextField baseTri = new JFormattedTextField();
		baseTri.setEditable(false);
		baseTri.setText("Base :");
		baseTri.setBounds(271, 407, 114, 19);
		getContentPane().add(baseTri);
		
		JLabel label = new JLabel("");
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setIcon(new ImageIcon("/home/etudiant/MDylan/P1_calcule_perim/src/v3/cercle.png"));
		label.setBounds(45, 215, 169, 129);
		getContentPane().add(label);
		
		JLabel label_1 = new JLabel("");
		label_1.setIcon(new ImageIcon("/home/etudiant/MDylan/P1_calcule_perim/src/v3/triangle.png"));
		label_1.setBounds(60, 382, 193, 185);
		getContentPane().add(label_1);
		
		JLabel label_2 = new JLabel("");
		label_2.setHorizontalAlignment(SwingConstants.CENTER);
		label_2.setIcon(new ImageIcon("/home/etudiant/MDylan/P1_calcule_perim/src/v3/carre.png"));
		label_2.setBounds(30, 36, 201, 136);
		getContentPane().add(label_2);
		
		JButton btnReintia = new JButton("Reinistialisation");
		btnReintia.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				affBaseTriangle.setText(null);
				affHauteurTriangle.setText(null);
				affRayonCercle.setText(null);
				affCoteCarre.setText(null);
				affPerimCar.setValue(null);
				affPerimTria.setValue(null);
				AffperimCerc.setValue(null);
				affSurfaceCar.setValue(null);
				affSurfaceCerc.setValue(null);
				affSurfaceTria.setValue(null);
			}
			
				
		});
		btnReintia.setBounds(100, 613, 153, 25);
		getContentPane().add(btnReintia);
		
		JButton btnNewButton_1 = new JButton("Calcule");
		btnNewButton_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				if(!affCoteCarre.getText().equals("")) 
				{
					resultat.add(new Rectangle(Float.valueOf(affCoteCarre.getText() ) ) );
					affPerimCar.setValue(resultat.get(num).getPerimetre());
					affSurfaceCar.setValue(resultat.get(num).getSurface());
					num++;
				}
				

			
				if(!affRayonCercle.getText().equals("")) 
				{
					resultat.add(new Cercle(Float.valueOf(affRayonCercle.getText())));
					AffperimCerc.setValue(resultat.get(num).getPerimetre());
					affSurfaceCerc.setValue(resultat.get(num).getSurface());
					num++;
				}
			

				//affSurfaceCar.setValue("");
			
				if(!affBaseTriangle.getText().equals("") & !affHauteurTriangle.getText().equals("")) 
				{
					resultat.add(new Triangle(Float.valueOf(affBaseTriangle.getText()), Float.valueOf(affHauteurTriangle.getText())));
					affPerimTria.setValue(resultat.get(num).getPerimetre());
					affSurfaceTria.setValue(resultat.get(num).getSurface());
					num++;
				}

				
				listeRegion.setText(null);
				for(OptimizV2 a : resultat) {
						listeRegion.setText(listeRegion.getText() + a.toString());
				}
				
			}
		});
		btnNewButton_1.setBounds(296, 613, 146, 25);
		getContentPane().add(btnNewButton_1);

		 
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 913, 752);
	}

	public static boolean BoolFloat(String f)
	{
		boolean b;
		float c=0;
		try
		{
			c = Float.valueOf(f);
			b=true;
		}
		catch(Exception e)
		{
			b = false;
			
		}
		c += c;
		return b;
	}
}
