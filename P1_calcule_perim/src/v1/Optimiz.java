package v1;

import java.util.Scanner;
import java.util.ArrayList;
import java.util.List;

public class Optimiz {
	public static Scanner set = new Scanner(System.in);
	static List<String> resultat = new ArrayList<>();
	static int num = 0;
	static boolean b = true;
	static int i;
	static String vidage;

	public static int IntegerTest(int i)
	{
		b=true;
		try
		{
			i = set.nextInt();
			//throw new Exception("Vous n'avez pas saisie un int!"+  i);
		}
		catch(Exception e)
		{
			System.out.println("Vous n'avez pas saisie un int!");
			b = false;
			vidage = set.next();
		}
		return i;
	}

	public static void Menu() {
		boolean correct = true;
		boolean sortie = true;
		Scanner choix = new Scanner(System.in);


		do {

			System.out.println("Choisir un chiffre en fonction du choix néccessaires :\n\t1)Carré\n\t"
					+ "2)Triange issocel\n\t3)Cercle\n\t9)Quitter");
			try{
				//i = choix.nextInt();

				i = choix.nextInt();


				if((i != 1) && ( i!=2) && (i!=3) && (i!=9))
				{
					correct = false;	
					System.out.println("Saisie incorrect");
				}
			}
			catch(Exception e)
			{
				System.out.println("Vous avez saisie une lettre ?!!");
				vidage = choix.next();
			}

			if(correct)
			{
				if (i == 9) {
					sortie = false;
				} else if (i == 1) {
					Carre();
				} else if (i == 2) {
					Triangle();
				} else if (i == 3) {
					Rond();
				}
			}
			correct = true;
		} while (sortie);

		choix.close();
		System.out.println("Calcul");

		for(String line : resultat)
		{
			System.out.println(line);
		}

		System.out.println("\nBonne journée !");
	}

	public static void Carre() {
		i = 0;

		System.out.println("Vous avez choisis le carré !");
		// Scanner set = new Scanner(System.in);
		do {
			System.out.println("\tVeuillez indiquer la taille de son côté");
			i = IntegerTest(i);
		}while(!b);
		System.out.println("\nSon périmètre est donc de " + (i * 4) + "cm.");
		System.out.println("\nSa surface est de " + (i * i) + "cm carré.");
		// set.close();
		num++;
		resultat.add(num + "   Carré\n\tPérimètre\t" + (i * 4) + "cm"+ "\n\tSurface\t\t"+ (i * i)+"cm carré");

	}

	public static void Triangle() {
		i = 0;
		int x = 0;
		// Scanner set = new Scanner(System.in);
		System.out.println(
				"\nLe calcule d'un triange isocèle.");
		do {
			System.out.println("Indiquez la première longeur connue (la base)");
			i = IntegerTest(i);

			System.out.println("\nPuis sa hauteur : ");

			x = IntegerTest(x);
		}while(!b);

		System.out.println("\nSon périmètre est donc de " + Math.sqrt(((x * x) + ((i / 2) * (i / 2)))*2+i) + "cm.");
		System.out.println("\nSa surface " + ((i * x) / 2) + "cm carré.");
		// set.close();
		num++;
		resultat.add(num + "   Triangle\n\tPérimètre\t\t" +  Math.sqrt(((x * x) + ((i / 2) * (i / 2)))*2+i)    + "cm"
				+ "\n\tSurface\t"+ ((i * x) / 2)+"cm carré");


	}

	public static void Rond() {
		b = true;
		// Scanner set = new Scanner(System.in);
		do {
			System.out.println("\nMaintenant pour un cercle, veuillez indiquer son rayon");
			i = IntegerTest(i);
		}while(!b);
		if(b){
			System.out.println("\nSon périmètre est donc de " + (2 * 3.14 * i) + "cm.");
			System.out.println("\nSa surface est donc de " + (3.14 * i * i) + "cm carré.");
			num++;
			resultat.add(num + "   Rond\n\tPérimètre\t\t" + (2 * 3.14 * i)  + "cm"
					+ "\n\tSurface\t"+ (3.14 * i * i)+"cm carré");
		}
	}
}
