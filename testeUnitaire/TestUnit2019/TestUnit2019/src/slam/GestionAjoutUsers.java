package slam;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import util.XPathUtil;

public class GestionAjoutUsers {
  /**
   * Logger for this class
   */
  private static final Log logger = LogFactory.getLog(GestionAjoutUsers.class);

  /**
   * le nom du fichier des users
   */
  private String fileName;

  /**
   * une collection de type dictionnaire : cle=>id, val=>User
   */
  private Map<String, User> lesUsers;

  /**
   * 
   * @param fileName
   *          le fichier des users E/S
   * @throws Exception
   *           en cas de pb E/S
   */
  public GestionAjoutUsers(String fileName) throws IOException {
    this.fileName = fileName;
    this.lesUsers = new HashMap<String, User>();
    loadFromFile();
  }

  /**
   * Place la liste XML des users dans une liste en mémoire.
   * 
   * @throws IOException
   */
  public void loadFromFile() throws IOException {

    lesUsers.clear();

    File f = new File(fileName);
    if (!f.exists()) {
      f.createNewFile();
      saveToFile();
    }

    URL url = f.toURI().toURL();

    String expressionXPath = "users/user";

    NodeList nodes = XPathUtil.eval(url.openStream(), expressionXPath);

    // itération sur les <user>
    for (int e = 0; e < nodes.getLength(); e++) {
      Node node = nodes.item(e);
      NodeList enfants = node.getChildNodes();
      String nom = "";
      String prenom = "";
      String id = "";
      String mdp = "";
      for (int i = 0; i < enfants.getLength(); i++) {
        String balise = enfants.item(i).getLocalName();
        String contenu = enfants.item(i).getTextContent().trim();
        if ("prenom".equals(balise))
          prenom = contenu;
        else if ("nom".equals(balise))
          nom = contenu;
        else if ("id".equals(balise))
          id = contenu;
        else if ("mdp".equals(balise))
          mdp = contenu;
      }
      lesUsers.put(id, new User(prenom, nom, id, mdp));
    }
  }

  /**
   * Enregistre les users sur disque
   * 
   * @throws IOException
   */
  public void saveToFile() throws IOException {
    FileOutputStream fo = new FileOutputStream(fileName);
    PrintWriter pw = new PrintWriter(fo);
    // toString redéfini pour générer du XML
    pw.print(this.toString());
    pw.close();
  }

  /**
   * Ajoute un utilisateur à la liste en cours
   * 
   * @param prenom
   *          prénom de l'utilisateur
   * @param nom
   *          nom de l'utilisateur
 * @return 
   */
  public User ajoutUser(String prenom, String nom) {
    String id = genereId(prenom, nom);
    String mdp = genereMotPass(8);
    User user = new User(prenom, nom, id, mdp);
    lesUsers.put(id, user);
    logger.trace(user);
    return user;
  }

  /*
   * 
   * @see java.lang.Object#toString()
   */
  public String toString() {
    StringBuffer res = new StringBuffer();
    res.append("<?xml version='1.0' ?>\n");
    res.append("<users>\n");

    Iterator<User> users = lesUsers.values().iterator();

    while (users.hasNext()) {
      res.append(users.next());
      res.append("\n");
    }

    res.append("</users>");
    return res.toString();
  }

  /**
   * Génération d'un id unique (par rapport à  la liste)
   * 
   * @param prenom
   *          prénom
   * @param nom
   *          nom
   * @return un id
   */
  public String genereId(String prenom, String nom) {	  
	String id = prenom + nom;
    return id;
  }

  /**
   * Génération d'un mot de passe
   * 
   * @param taille
   *          longueur souhaitée pour le mdp
   * @return un mot de passe
   */
  public String genereMotPass(int taille) {
	  String mdp="";
	  Random rd = new Random();
	  for (int i = 0; i<=taille;i++)
	  {
		  mdp+=""+rd.nextInt(10); 
	  }
	  return mdp;
  }

}
