package slam;

public class User {

	private String prenom;
	private String nom;
	private String id;
	private String mdp;

	public User() {
		super();
	}
 
	public User(String prenom, String nom, String id, String mdp) {
		this.prenom=prenom;
		this.nom=nom;
		this.id=id;
		this.mdp=mdp;
	}
	
	public String toString(){
		StringBuffer res = new StringBuffer();
		res.append("<user>");
		res.append("<prenom>");
		res.append(this.prenom);
		res.append("</prenom>");
		res.append("<nom>");
		res.append(this.nom);
		res.append("</nom>");
		res.append("<id>");
		res.append(this.id);
		res.append("</id>");
		res.append("<mdp>");
		res.append(this.mdp);
		res.append("</mdp>");
		res.append("</user>");
		return res.toString();		
	}

	public String getId() {
		return this.id;
	}
}
