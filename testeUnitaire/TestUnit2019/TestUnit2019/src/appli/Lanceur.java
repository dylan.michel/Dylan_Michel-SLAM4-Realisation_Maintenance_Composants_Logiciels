package appli;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import slam.Vue;

public class Lanceur {
	/**
	 * Logger for this class
	 */
	private static final Log logger = LogFactory.getLog(Lanceur.class);

	/**
	 * Lanceur
	 * 
	 * @param args
	 *            args[0] le nom du fichier XML des utilisateurs (defaut = users.xml)
	 */
	static public void main(String[] args) {
		try {
			Vue appli = new Vue("users.xml");//fichier xml
			appli.run();
		} catch (Exception e) {
			logger.fatal(e.getMessage());
		}
	}

}
