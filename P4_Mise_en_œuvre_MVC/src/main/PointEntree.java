package main;

import vue.ControleurPrincipal;
import vue.laVue;

public class PointEntree {

	public static void main(String[] args) {
		ControleurPrincipal leControleur = new ControleurPrincipal();

		laVue frame = new laVue(leControleur);
		
		leControleur.setVue(frame);

	}

}
