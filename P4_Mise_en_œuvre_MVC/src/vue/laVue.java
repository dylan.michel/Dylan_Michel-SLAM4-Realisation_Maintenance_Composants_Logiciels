package vue;

import java.awt.Button;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import classes.Club;
import dao.ClubDAO;
import dao.LicencieDAO;
import classes.Licencie;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JLabel;

public class laVue  extends JFrame{
	
	protected ControleurPrincipal controleur;
	private JPanel contentPane;
	private JTextField TextInfoClub;
	private JCheckBox chckbxOnLicencie;
	JComboBox<String> CBXListclub;
	ClubDAO DAOclub = new ClubDAO();
	JTextArea ListLicence;
	
	

	public void MajListClub(List<Club> lesClub) 
	{
		CBXListclub.addItem("Selectionné un club");
		//CBXListclub.removeAllItems();
		for (Club c : lesClub)
		{
			//CBXListclub.addItem(c);
			CBXListclub.addItem("Code du club : " + c.getCode() + "   --   Nom du club :" + c.getNom());
		}
	};
	
	public boolean leChoix() 
	{
		return chckbxOnLicencie.isSelected();
	}
	
	public Club choixDeClub(List<Club> lesClub) 
	{
		Club selection;
		CleanInfoClub();
		if(CBXListclub.getSelectedIndex() != 0)
		{
			selection = lesClub.get(CBXListclub.getSelectedIndex()-1);
			TextInfoClub.setText(selection.toString());
			
			
			return selection;
		}
		else
		{
			selection = new Club("","","","");
			return selection;
		}
		
		
	}
	public void UpdateListLicencie(List<Licencie> lesLicencie) 
	{
		CleanListLicencie();
	
		if(CBXListclub.getSelectedIndex() != 0)
		{
			for(Licencie a : lesLicencie) 
			{
			ListLicence.setText(ListLicence.getText()+a.toStringsansClub());
			}
		}
		
	}
	public void CleanListLicencie()
	{
	ListLicence.removeAll();	
	}
	public void CleanInfoClub() 
	{
		TextInfoClub.setText("");
		ListLicence.removeAll();
	}
	
	public laVue(ControleurPrincipal unControleur)
	{
		
		this.controleur = unControleur;
		
		/**
		 * Create the frame.
		 */
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 700, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setVisible(true);
		
		TextInfoClub = new JTextField();
		TextInfoClub.setEditable(false);
		TextInfoClub.setBounds(50, 100, 600, 25);
		contentPane.add(TextInfoClub);
		TextInfoClub.setColumns(10);
		
		
		chckbxOnLicencie = new JCheckBox("On licencie");
		chckbxOnLicencie.setBounds(100, 525, 129, 23);
		contentPane.add(chckbxOnLicencie);
		
		CBXListclub = new JComboBox<String>();
		CBXListclub.setBounds(50, 50, 600, 25);
		contentPane.add(CBXListclub);
		CBXListclub.setActionCommand("SelectionClub");
		CBXListclub.addActionListener(controleur);
		
		Button Ajouter = new Button("Ajouter");
		chckbxOnLicencie.setActionCommand("Ajouter");
		Ajouter.addActionListener(controleur);
		Ajouter.setBounds(100, 450, 250, 50);
		contentPane.add(Ajouter);
		
		Button Supprimer = new Button("Supprimer");
		Supprimer.setBounds(225, 500, 250, 50);
		Supprimer.setActionCommand("Supprimer");
		Supprimer.addActionListener(controleur);
		contentPane.add(Supprimer);
		
		Button Modifier = new Button("Modifier");
		Modifier.setBounds(350, 450, 250, 50);
		Modifier.setActionCommand("Modifier");
		Modifier.addActionListener(controleur);
		contentPane.add(Modifier);
		
		ListLicence = new JTextArea();
		ListLicence.setBounds(50, 150, 600, 280);
		contentPane.add(ListLicence);
		
		
		JButton btnQuitter = new JButton("Quitter");
		btnQuitter.setBounds(536, 525, 114, 25);
		btnQuitter.setActionCommand("Quitter");
		btnQuitter.addActionListener(controleur);
		contentPane.add(btnQuitter);
		
	}
}
