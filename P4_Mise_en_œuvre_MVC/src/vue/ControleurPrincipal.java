package vue;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import classes.Club;
import classes.Licencie;
import dao.ClubDAO;
import dao.ConnexionPostgreSql;
import dao.DAO;
import dao.LicencieDAO;

public class ControleurPrincipal implements ActionListener, WindowListener {
	
	private laVue vue;
	
	private DAO<Club> gestionClub;
	private DAO<Licencie> gestionLicencie;
	private List<Club> listClub;
	private List<Licencie> listLicencie=new ArrayList<Licencie>();
	private Club club;
	
	public ControleurPrincipal()
	{
		this.gestionClub=new ClubDAO();
		this.gestionLicencie = new LicencieDAO();
		
		
		listClub = gestionClub.recupAll();
	}
	
	public void setVue(laVue view)
	{
		this.vue = view;
		
		this.vue.MajListClub(listClub);
		
		
	}
	
	
	
	public void actionPerformed(ActionEvent e) 
	{
		if(e.getActionCommand().equals("Quit")) 
		{
			try {
				gestionClub.connect.close();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			try {
				gestionLicencie.connect.close();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			System.exit(1);
		}
		else if(e.getActionCommand().equals("Ajouter")) 
		{
			if(vue.leChoix())
			{
				
			}
			else
			{
				
			}
		}
		else if(e.getActionCommand().equals("Modifier")) 
		{
			if(vue.leChoix())
			{
				
			}
			else
			{
				
			}
		}
		else if(e.getActionCommand().equals("Supprimer")) 
		{
			if(vue.leChoix())
			{
				
			}
			else
			{
				
			}
		}

		else if(e.getActionCommand().equals("Quitter")) 
		{
			System.exit(0);
		}
		else if(e.getActionCommand().equals("SelectionClub"))
		{
			
			
			club = vue.choixDeClub(listClub);

			if (club.getCode() != "")
			{
				if(!listLicencie.isEmpty())
				{
				listLicencie.clear();
				}

			listLicencie = gestionLicencie.recupAllLicencie(club.getCode());
			vue.UpdateListLicencie(listLicencie);
			}
		}
	}

	@Override
	public void windowActivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosed(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosing(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeactivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeiconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowIconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowOpened(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
}
