package dao;

import java.sql.Connection;
import java.util.List;

import classes.Club;
import classes.Licencie;

/**
 * Classe utilisant le CRUD
 * @author Ryokin
 *
 * @param <T> Parametre qui permet de definir en fonction du besoin, la classe neccesaire
 */
public abstract class DAO<T> {
	
	/**
	 * Permet de crée une instance de connexion à la base de donnée entrée précedement dans ConnexionPostgreSql
	 */
	public static Connection connect = ConnexionPostgreSql.getInstance();
	
	/**
	 * Méthode abstraite qui permetra de crée un tuple dans la base de donnée, de type T 
	 * @param obj Objet ciblé
	 */
	public abstract void create(T obj);
	
	/**
	 * Méthode abstraite qui permetra de lire un tuple dans la base de donnée, de type T 
	 * @param obj Objet ciblé
	 * @return Les informations de l'objet
	 */
	public abstract T read(String code);
	
	/**
	 * Méthode abstraite qui permetra de mettre à jour un tuple dans la base de donnée, de type T 
	 * @param obj Objet ciblé
	 */
	public abstract void update(T obj);
	
	/**
	 * Méthode abstraite qui permetra de supprimer un tuple dans la base de donnée, de type T 
	 * @param obj Objet ciblé
	 */
	public abstract void delete(T obj);

	public abstract List<T> recupAll();

	public List<Licencie> recupAllLicencie(String code) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	
}
