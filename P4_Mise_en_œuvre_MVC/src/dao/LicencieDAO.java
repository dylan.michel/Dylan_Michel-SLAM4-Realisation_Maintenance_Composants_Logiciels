package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import classes.Club;
import classes.Licencie;

public class LicencieDAO extends DAO<Licencie>  {

	
	@Override
	public List<Licencie> recupAll() {
		// définition de la liste qui sera retournée en fin de méthode
		List<Licencie> listeLicencie = new ArrayList<Licencie>(); 
		
		// déclaration de l'objet qui servira pour la requète SQL
		try {
			Statement requete = this.connect.createStatement();

			// définition de l'objet qui récupère le résultat de l'exécution de la requète
			ResultSet curseur = requete.executeQuery("select * from \"mvc\".licencie");

			// tant qu'il y a une ligne "résultat" à lire
			while (curseur.next()){
				// objet pour la récup d'une ligne de la table Club
				Licencie unLicencie = new Licencie();
				unLicencie.setCode(curseur.getString("code"));
				unLicencie.setNom(curseur.getString("nom"));
				unLicencie.setPrenom(curseur.getString("prenom"));
				unLicencie.setDateNaiss(curseur.getString("datenaiss"));
				unLicencie.setLeClub(curseur.getString("leclub"));
				
				listeLicencie.add(unLicencie);
			}
					
			curseur.close();
			requete.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} 

		return listeLicencie; 
	}
	
	
	public List<Licencie> recupAllLicencie(String code)
	{
		List<Licencie> listeLicencie = new ArrayList<Licencie>();
		
		
		// déclaration de l'objet qui servira pour la requète SQL
		try {
			Statement requete = this.connect.createStatement();

			// définition de l'objet qui récupère le résultat de l'exécution de la requète
			ResultSet curseur = requete.executeQuery("select * from \"mvc\".licencie where leclub = '" + code +"'");
			// tant qu'il y a une ligne "résultat" à lire
			while (curseur.next()){
				// objet pour la récup d'une ligne de la table Club
				Licencie unLicencie = new Licencie();
				
				unLicencie.setCode(curseur.getString("code"));
				unLicencie.setNom(curseur.getString("nom"));
				unLicencie.setDateNaiss(curseur.getString("datenaiss"));
				unLicencie.setPrenom(curseur.getString("prenom"));
				unLicencie.setLeClub(curseur.getString("leclub"));
				listeLicencie.add(unLicencie);
			}
					
			curseur.close();
			requete.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} 
		
	
		return listeLicencie;
	}
	
	
	// Insertion d'un objet Club dans la table Club (1 ligne)
	@Override
	public void create(Licencie obj){
		try {
			 	PreparedStatement prepare = this.connect
	                                        .prepareStatement("INSERT INTO \"mvc\".licencie VALUES(?, ?, ?, ?, ?)"
	                                         );
			 	prepare.setString(1,obj.getCode());
				prepare.setString(2, obj.getNom());
				prepare.setString(3, obj.getPrenom());
				prepare.setString(4, obj.getDateNaiss());
				prepare.setString(5, obj.getLeClub());
				
					
				prepare.executeUpdate();  
					
			}
		catch (SQLException e) {
		e.printStackTrace();
		} 
	}
		
		
	// Recherche d'un Licencie par rapport à son code	
	@Override
	public Licencie read(String code) {
	
		Licencie leLicencie = new Licencie();
		Club unClub = new Club();
		try {
	          ResultSet result = this.connect
	                             .createStatement( 	ResultSet.TYPE_SCROLL_INSENSITIVE, 
	                                                ResultSet.CONCUR_UPDATABLE)
	                             .executeQuery("SELECT * FROM \"mvc\".licencie WHERE code = '" + code +"'");
	          
	          if(result.first())
	        	  leLicencie = new Licencie(code, result.getString("nom"),result.getString("prenom"),result.getString("datenaiss")/*,result.getObject(unClub)*/);   
	    }
		catch (SQLException e) {
			        e.printStackTrace();
		}
		return leLicencie;
		
	}
		
	
	// Mise à jour d'un Licencie
	@Override
	public void update(Licencie obj) {
		try { this .connect	
	               .createStatement( ResultSet.TYPE_SCROLL_INSENSITIVE,
	            		             ResultSet.CONCUR_UPDATABLE )
	               .executeUpdate("UPDATE \"mvc\".licencie SET nom = '" + obj.getNom() 
	               + "', prenom ='" + obj.getPrenom() +
	               "',datenaiss = '" + obj.getDateNaiss() +
	               "',leclub ='" + obj.getLeClub() + "' " +
	                    	      " WHERE code = '" + obj.getCode()+"'" );
		}
		catch (SQLException e) {
		      e.printStackTrace();
		}
		
	}


	// Suppression d'un Licencie
	@Override
	public void delete(Licencie obj) {
		try {
	           this.connect
	               .createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
	            		            ResultSet.CONCUR_UPDATABLE)
	               .executeUpdate("DELETE FROM \"mvc\".licencie WHERE code = '" + obj.getCode()+"'");
				
		 }
		catch (SQLException e) {
		            e.printStackTrace();
		}
	}

	
	
	
	
	
}
