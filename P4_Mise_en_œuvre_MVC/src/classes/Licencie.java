package classes;

public class Licencie {
	
	/**
	 * L'attribut code est la valeur qui permettra de declarer la clé primaire de la base de donnée.
	 */
	private String code ;
	/**
	 * L'attribut nom represente le nom du licencié.
	 * 
	 */
	private String nom ;
	
	/**
	 * L'attribut prenom represente le prenom du licencié.
	 */
	private String prenom;
	
	/**
	 * L'attribut dateNaiss represente le dateNaiss du licencié.
	 */
	private String dateNaiss;
	
	/**
	 * L'attribut leClub representera le club au quel est attaché le licencie ou pas.
	 */
	private String leClub;
	
	/**
	 * Permet de retourner l'attribut code de l'objet.
	 * @return Retourne l'attribut code qui est la valeur qui permettra de declarer la clé primaire de l'objet dans la base de donnée.
	 */
	public String getCode() 
	{
		return code;
	}
	
	/**
	 * Permet de modifier l'attribut code de l'objet
	 * @param _code est une valeur de type STRING qui va remplacer l'attribut code de l'objet.
	 */
	public void setCode(String _code) 
	{
		code = _code;
	}

	/**
	 * Permet de retourner l'attribut nom de l'objet.
	 * @return Retourne l'attribut nom qui represente le nom du club.
	 */
	public String getNom() 
	{
		return nom;
	}

	/**
	 * Permet de modifier l'attribut nom de l'objet
	 * @param _nom est une valeur de type STRING qui va remplacer l'attribut nom de l'objet.
	 */
	public void setNom(String _nom) 
	{
		nom = _nom;
	}

	
	/**
	 * Permet de retourner l'attribut prenom de l'objet.
	 * @return Retourne l'attribut prenom de l'objet Licencie.
	 */
	public String getPrenom() {
		return prenom;
	}

	/**
	 * Permet de modifier l'attribut prenom de l'objet
	 * @param _nom est une valeur de type STRING qui va remplacer l'attribut prenom de l'objet.
	 */	
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	
	/**
	 * Permet de retourner l'attribut dateNaiss de l'objet.
	 * @return Retourne l'attribut dateNaiss de l'objet Licencie.
	 */
	public String getDateNaiss() {
		return dateNaiss;
	}

	/**
	 * Permet de modifier l'attribut dateNaiss de l'objet
	 * @param _nom est une valeur de type STRING qui va remplacer l'attribut dateNaiss de l'objet.
	 */
	public void setDateNaiss(String dateNaiss) {
		this.dateNaiss = dateNaiss;
	}

	/**
	 * Permet de retourner l'attribut leClub de l'objet.
	 * @return Retourne l'attribut leClub de l'objet Licencie.
	 */
	public String getLeClub() {
		return leClub;
	}

	/**
	 * Permet de modifier l'attribut leClub de l'objet
	 * @param _nom est une valeur de type CLUB qui va remplacer l'attribut leClub de l'objet.
	 */
	public void setLeClub(String leClub) {
		this.leClub = leClub;
	}

	/**
	 * Constucteur par defaut de la classe licencie
	 */
	public Licencie() {
		this.code = "0";
		this.nom = "Caesar";
		this.prenom = "Jules";
		this.dateNaiss = "01/01/0000";
		
	}

	/**
	 * Constructeur disposant de tout les parametres de la classe sauf leClub
	 * @param _code L'attribut code est la valeur qui permettra de declarer la clé primaire de la base de donnée.
	 * @param _nom L'attribut nom represente le nom du licencié.
	 * @param _prenom L'attribut prenom represente le prenom du licencié.
	 * @param _dateNaiss L'attribut leClub representera le club au quel est attaché le licencie ou pas.
	 */
	public Licencie(String _code, String _nom, String _prenom, String _dateNaiss) {
		
		this.code = _code;
		this.nom = _nom;
		this.prenom = _prenom;
		this.dateNaiss = _dateNaiss;
	}

	/**
	 * Constructeur disposant de tout les parametres de la classe
	 * @param _code L'attribut code est la valeur qui permettra de declarer la clé primaire de la base de donnée.
	 * @param _nom L'attribut nom represente le nom du licencié.
	 * @param _prenom L'attribut prenom represente le prenom du licencié.
	 * @param _dateNaiss L'attribut leClub representera le club au quel est attaché le licencie ou pas.
	 * @param _leClub L'attribut leClub representera le club au quel est attaché le licencie ou pas.
	 */
	public Licencie(String _code, String _nom, String _prenom, String _dateNaiss, String _leClub) {
		
		this.code = _code;
		this.nom = _nom;
		this.prenom = _prenom;
		this.dateNaiss = _dateNaiss;
		this.leClub = _leClub;
	}

	/**
	 * Méthode qui retourne les informations du licencié en fonction de s'il possède un club ou non
	 * @return if Si la personne ne possède pas de club alors cela ne retourne pas les infos clubs
	 * @return else Si la personne possède un club alors on a les infos club avec celle du licencié
	 */
	@Override
	public String toString() {
		
		if(leClub == null)
		{
		return "Licencie [code=" + code + ", nom=" + nom + ", prenom=" + prenom + ", dateNaiss=" + dateNaiss + "\n";
		}
		else 
		{
		return "Licencie [code=" + code + ", nom=" + nom + ", prenom=" + prenom + ", dateNaiss=" + dateNaiss
					+ ", leClub=" + leClub + "]" +"\nInfo club :" + leClub.toString();
		}
	}
	
	public String toStringsansClub() {
		
		{
		return "code=" + code + "," + nom + "," + prenom + ", " + dateNaiss +"\n";
		}

	}
	
	
	

}
