package classes;
import java.lang.String;

public class Club {

	/**
	 * L'attribut code est la valeur qui permettra de declarer la clé primaire de la base de donnée.
	 */
	private String code ;
	/**
	 * L'attribut nom represente le nom du club.
	 * 
	 */
	private String nom ;
	/**
	 * L'attribut nom_president represente le nom président du club.
	 */
	private String nom_president; 
	/**
	 * L'attribut nom_entraineur represente le nom de l'entraineur du club.
	 */
	private String nom_entraineur;
	
	
	
	/**
	 * Permet de retourner l'attribut code de l'objet.
	 * @return Retourne l'attribut code qui est la valeur qui permettra de declarer la clé primaire de la base de donnée.
	 */
	public String getCode() 
	{
		return code;
	}
	
	/**
	 * Permet de modifier l'attribut code de l'objet
	 * @param _code est une valeur de type STRING qui va remplacer l'attribut code de l'objet.
	 */
	public void setCode(String _code) 
	{
		code = _code;
	}

	/**
	 * Permet de retourner l'attribut nom de l'objet.
	 * @return Retourne l'attribut nom qui represente le nom du club.
	 */
	public String getNom() 
	{
		return nom;
	}

	/**
	 * Permet de modifier l'attribut nom de l'objet
	 * @param _nom est une valeur de type STRING qui va remplacer l'attribut nom de l'objet.
	 */
	public void setNom(String _nom) 
	{
		nom = _nom;
	}

	/**
	 * Permet de retourner l'attribut nom_president de l'objet.
	 * @return Retourne l'attribut nom qui represente le nom_president du club.
	 */
	public String getNom_president() 
	{
		return nom_president;
	}

	/**
	 * Permet de modifier l'attribut nom_president de l'objet
	 * @param _nom_president est une valeur de type STRING qui va remplacer l'attribut nom_president de l'objet.
	 */
	public void setNom_president(String _nom_president) 
	{
		nom_president = _nom_president;
	}

	/**
	 * Permet de retourner l'attribut nom_entraineur de l'objet.
	 * @return Retourne l'attribut nom qui represente le nom_entraineur du club.
	 */
	public String getNom_entraineur() 
	{
		return nom_entraineur;
	}

	/**
	 * Permet de modifier l'attribut nom_entraineur de l'objet
	 * @param _nom_entraineur est une valeur de type STRING qui va remplacer l'attribut nom_entraineur de l'objet.
	 */
	public void setNom_entraineur(String _nom_entraineur) 
	{
		nom_entraineur = _nom_entraineur;
	}

	/**
	 * Constructeur par defaut qui ne pourra être implementer dans la base de donner seulement 1 fois
	 */
	public Club() {
		code = "1";
		nom = "Le nom";
		nom_president = "Le nom president";
		nom_entraineur = "Le nom d entraineur";
	}

	/**
	 * Constructeur qui comporte tout les attributs de la classe
	 * @param L'attribut code est la valeur qui permettra de declarer la clé primaire de la base de donnée.
	 * @param L'attribut nom indique le nom du club.
	 * @param L'attribut nom_president indique le nom du président du club.
	 * @param L'attribut nom_entraineur indique le nom de l'entraineur du club.
	 */
	public Club(String code, String nom, String nom_president, String nom_entraineur) {
		this.code = code;
		this.nom = nom;
		this.nom_president = nom_president;
		this.nom_entraineur = nom_entraineur;
	}
	
	/**
	 * Constructeur qui permet de crée un club à partir d'un club déjà existant
	 * @param leClub Attribut Club
	 */
	public Club(Club leClub)
	{
		this.code = leClub.code;
		this.nom = leClub.nom;
		this.nom_president = leClub.nom_president;
		this.nom_entraineur = leClub.nom_entraineur;
	}

	/**
	 * Permet de retourner les valeurs des attributs de la classe club.
	 */
	@Override
	public String toString() {
		return "Club [code=" + code + ", nom=" + nom + ", nom_president=" + nom_president + ", nom_entraineur="
				+ nom_entraineur + "]\n";
	}
	
	
	
	
}
