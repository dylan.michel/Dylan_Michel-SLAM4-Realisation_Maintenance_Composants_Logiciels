package v0;

public class Main {
	static int bureau =9;
	public static void main(String[] args) 
	{
		System.out.println("Debout Gaston ! Il faut trier le courrier !");
		GastonLagaffe.trierCourrierEnRetard(1);
		
		try
		{
			System.out.println("Va ranger ton bureau !");
		GastonLagaffe.rangerBureau();
		}
		catch(Exception e)
		{
			if(bureau > 10) {
			System.out.println( e.getMessage() );
			}
			else
				System.out.println("Ouf, j'ai fini.");
		}
		System.out.println("La sieste s'impose.");
		
		System.out.println("DingDong !\nGaston réveillez vous ! Mr. Demesmaeker arrive, faites vite !");
		GastonLagaffe.faireSignerContrats();
		
		GastonLagaffe.appeler("Mr Boulier");
		GastonLagaffe.appeler("Prunelle");
		GastonLagaffe.appeler("Enzo");
		
		GastonLagaffe.preparerJournal();
	}

}
