package v0;

public class GastonLagaffe {
	public static void trierCourrierEnRetard(int nbLettres)
	{
	System.out.print("Quoi, " + nbLettres + " lettre(s) à trier ? ");
	try
	{
		System.out.println("OK, OK, je vais m'y mettre...");
		if(nbLettres > 2)
			{
			throw new Exception("Beaucoup trop de lettres...");
			}
		System.out.println("Ouf, j'ai fini.");
	}
	catch(Exception e)
	{
		System.out.println("M'enfin ! " + e.getMessage());
	}
	System.out.println("Après tout ce travail, une sieste s'impose.");
	}
	
	public static void rangerBureau() throws Exception
	{
	System.out.println("Ranger mon bureau ? Si tu insistes...");
	throw new Exception("Impossible, l'armoire est déjà pleine !");
	}
	public static void faireSignerContrats()
	{
	try
	{
	System.out.println("Encore ces contrats ? OK, je les imprime...");
	imprimerContrats();
	System.out.println("A présent une petite signature...");
	ajouterSignature();
	System.out.println("Fantasio, les contrats sont signés !");
	}
	catch (Exception e) {
	System.out.println("M'enfin ! " + e.getMessage());
	}
	}
	private static void ajouterSignature() {
	System.out.println("Signez ici, M'sieur Demesmaeker.");
	}
	private static void imprimerContrats() throws Exception {
	System.out.println("D'abord, mettre en route l'imprimante.");
	allumerImprimante();
	System.out.println("Voilà, c'est fait !");
	}
	private static void allumerImprimante() throws Exception {
	System.out.println("Voyons comment allumer cette machine...");
	throw new Exception("Mais qui a démonté tout l'intérieur ?");
	}
	
	public static void repondreAuTelephone(String appelant)
	throws ExceptionMenfin, ExceptionBof
	{
	if(appelant.equals("Mr Boulier"))
	{
	throw new ExceptionMenfin("Je finis un puzzle.");
	}
	else
	if(appelant.equals("Prunelle"))
	{
	throw new ExceptionBof("Pas le temps, je suis dé-bor-dé !");
	}
	else
	{
	System.out.println("Allô, ici Gaston, j'écoute...");
	}
	}
	public static void appeler(String appelant)
	{
	System.out.println("Gaston, " + appelant + " au téléphone !");
	try
	{
	GastonLagaffe.repondreAuTelephone(appelant);
	}
	catch(Exception e)
	{
	System.out.println("Encore une bonne excuse, j'imagine ?");
	System.out.println(e.getMessage());
	}
	}
	
	public static void commanderFournitures() throws Exception
	{
	System.out.println("D'abord, réchauffer ma morue aux fraises...");
	System.out.println("Heureusement, j'ai réparé mon réchaud à gaz.");
	throw new Exception("Vite, où est l'extincteur ??");
	}
	
	public static void preparerJournal()
	{
	System.out.println("Gaston, une commande urgente !");
	try
	{
	commanderFournitures();
	}
	catch(Exception e)
	{
		System.out.println(e.getMessage());
	}
	System.out.println("Déjà terminé ? Il progresse, ce petit...");
	}
}
